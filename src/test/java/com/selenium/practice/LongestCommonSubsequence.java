package com.selenium.practice;

public class LongestCommonSubsequence {
	public int longestCommonSubsequence(String text1, String text2) {
		char[] ch1 = text1.toCharArray();
		char ch2[] = text2.toCharArray();
		int count = 0;
		for(int i=0;i<ch1.length;i++) {
			for(int j=0;j<ch2.length;j++) {
				if(ch1[i]==ch2[j]) {
					count++;
				}
			}
		}
		return count;
	}
	public static void main(String[] args) {
		LongestCommonSubsequence lcs = new LongestCommonSubsequence();
		String text1 = "ezupkr";
		String text2 = "ubmrapg";
		System.out.println(lcs.longestCommonSubsequence(text1, text2));
	}
}