package com.selenium.practice;

public class IsSubsequence {
	public boolean isSubsequence(String s, String t) {
		boolean b1;
		char[] ch1 = s.toCharArray();
		char ch2[] = t.toCharArray();
		String s1 = "";

		for(int i=0;i<ch1.length;i++) {
			for(int j=0;j<ch2.length;j++) {
				if(ch1[i]==ch2[j]) {
					s1 += ch2[j];
				}
			}
		}
		if(s1.equals(s)) {
			b1 = true;
		}else {
			b1 = false;
		}
		return b1;
	}
	public static void main(String[] args) {
		IsSubsequence seq = new IsSubsequence();
		String s = "acb";
		String t = "ahbgdc";
		System.out.println(seq.isSubsequence(s, t));
	}
}