package com.selenium.practice;

import java.util.*;

public class RemoveElement {
	public List<Integer> removeElement(int[] nums, int val) {
		List<Integer> list = new ArrayList<Integer>();
		for(int i=0;i<nums.length;i++) {
			if(val==nums[i]) {
				continue;
			}else {
				list.add(nums[i]);
			}
		}
		return list;
	}
	public static void main(String[] args) {
		RemoveElement remove = new RemoveElement();
		int[] n = {3,2,2,3};
		int val = 3;
		System.out.println(remove.removeElement(n, val));
	}
}