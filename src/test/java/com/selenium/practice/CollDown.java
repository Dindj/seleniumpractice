package com.selenium.practice;

import java.util.*;

public class CollDown {
	public int maxProfit(int[] prices) {
		String s1[] = {"buy","sell","colldown"};
		Map<String, Integer> map = new LinkedHashMap<>();
		for(int i=0;i<prices.length;i++) {
			if(i==0) {
				map.put("buy", prices[i]);
			}else if(i/2==1) {
				map.put("sell", prices[i]);
			}else if(i/2==0) {
				map.put("colldown", prices[i]);
			}
		}
		System.out.println(map);
		return 0;
	}
	public static void main(String[] args) {
		CollDown colldown = new CollDown();
		int n[] = {1,2,3,0,2};
		colldown.maxProfit(n);
	}
}