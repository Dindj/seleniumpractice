package com.selenium.practice;

import java.util.Arrays;

public class AddTwoNumberSumandCarry {
	public int[] addTwoNumber(int[] n1, int[] n2, int k) {
		int n3[] = new int[k];
		int sum = 0;
		for(int i=0;i<n1.length;i++) {
			for(int j=i;j<n2.length;) {
				n3[i] = (n1[i] + n2[j]) + sum;
				break;
			}
			sum = 0;
			String s1 = String.valueOf(n3[i]);
			if(s1.length()==2) {
				String[] s2 = s1.split("(?<=\\G.{1})");
				n3[i] = Integer.parseInt(s2[1]);
				sum = sum + Integer.parseInt(s2[0]);
			}
		}
		return n3;
	}
	public static void main(String args[]) {
		AddTwoNumberSumandCarry twonum = new AddTwoNumberSumandCarry();
		int n1[] = {9,9,9,9,9,9,9};
		int n2[] = {9,9,9,9};
		int k = n1.length+1;
		System.out.println(Arrays.toString(twonum.addTwoNumber(n1,n2,k)));
	}
}