package com.selenium.practice;

import java.util.*;

public class Remove_Duplicates_from_Sorted_Array {
	public int removeDuplicates(int[] nums) {
		Set<Integer> set = new LinkedHashSet<Integer>();
		for(int i=0;i<nums.length;i++) {
			for(int j=i+1;j<nums.length;j++) {
				if(nums[i]==nums[j]) {
					set.add(nums[i]);
				}else {
					set.add(nums[j]);
				}
			}
		}
		return set.size();
	}
	public static void main(String[] args) {
		Remove_Duplicates_from_Sorted_Array remove = new Remove_Duplicates_from_Sorted_Array();
		int n1[] = {1,1,2};
		System.out.println(remove.removeDuplicates(n1));
	}
}