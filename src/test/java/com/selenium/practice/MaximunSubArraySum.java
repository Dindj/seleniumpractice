package com.selenium.practice;

import java.util.*;

public class MaximunSubArraySum {
	public int maxSubArray(int[] nums) {
		List<Integer> list3 = new ArrayList<Integer>();
		int sum = 0;
		if(nums.length==2 || nums.length==1) {
			for(int i=0;i<nums.length;i++) {
				sum = sum + nums[i];
			}
			return sum;
		}else {
			List<List<Integer>> list1 = new ArrayList<List<Integer>>();
			int k = nums.length/2;
			for(int i=0;i<nums.length;i++) {
				List<Integer> list2 = new ArrayList<Integer>();
				for(int j=i;j<nums.length;j++) {
					list2.add(nums[j]);
					if(list2.size()==k) {
						break;
					}
				}
				list1.add(list2);
			}
			for(int i=0;i<list1.size();i++) {
				sum = 0;
				for(int j=0;j<list1.get(i).size();j++) {
					sum = sum + list1.get(i).get(j);
				}
				list3.add(sum);
			}
			return Collections.max(list3);
		}
	}
	public static void main(String []args) {
		MaximunSubArraySum subarray = new MaximunSubArraySum();
		int n[] = {-2,1,-3,4,-1,2,1,-5,4};
		System.out.println(subarray.maxSubArray(n));
	}
}