package com.selenium.practice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
//import io.github.bonigarcia.wdm.WebDriverManager;

public class MakeMyTrip {
	public WebDriver driver;
	public static void wait(int n) {
		try {
			Thread.sleep(n * (1000));
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public MakeMyTrip launchChromeBrowser() {
		try {
			System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
			//WebDriverManager.chromedriver().arch32().setup();
			ChromeOptions co = new ChromeOptions();
			co.addArguments("--incognito");
			driver = new ChromeDriver(co);
			driver.navigate().to("https://www.makemytrip.com/");
			driver.manage().window().maximize();
			wait(5);
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return this;
	}
	public MakeMyTrip closeChromeBrowser() {
		wait(3);
		driver.close();
		driver.quit();
		return this;
	}
	public MakeMyTrip make_my_trip() {
		return new MakeMyTrip()
				.launchChromeBrowser();
				//.closeChromeBrowser();
	}
	public static void main(String[] args) {
		MakeMyTrip makemytrip = new MakeMyTrip();
		makemytrip.make_my_trip();
	}
}