package com.selenium.practice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Google {
	public WebDriver driver;
	public void google() {
		try {
			System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
			driver = new ChromeDriver(new ChromeOptions().addArguments("-incognito"));
			driver.get("https://www.google.com/");
			driver.manage().window().maximize();
			Thread.sleep(8000);
			System.out.println("Successfully Chrome Launched...");
			System.out.println("The URL is   : " + driver.getCurrentUrl());
			System.out.println("The Title is : " + driver.getTitle());
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public static void main(String[] args) {
		new Google().google();
		System.out.println("Success...!!!");
	}
}