package com.selenium.record;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumRecording {
	public WebDriver driver;
	@BeforeTest
	public void initialize() {
		WebDriverManager.chromedriver().arch32().setup();
		driver = new ChromeDriver();
	}
	@AfterTest
	public void finish() {
		AvitoMP4Conversion.avitomp4("./test-recording/"+ScreenRecorderUtil.path, "./DataFile/"+ScreenRecorderUtil.path+".mp4");
		driver.close();
		driver.quit();
	}

	@Test
	public void google() throws Exception {
		ScreenRecorderUtil.startRecord("Google");
		driver.get("https://www.google.com/");
		driver.manage().window().maximize();
		driver.findElement(By.name("q")).sendKeys("Screen Recording");
		new Actions(driver).sendKeys(Keys.ENTER).build().perform();
		ScreenRecorderUtil.stopRecord();
	}
}