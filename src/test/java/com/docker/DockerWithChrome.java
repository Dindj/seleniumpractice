package com.docker;

import java.net.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DockerWithChrome {
	public static void main(String[] args) {
		try {

			URL url = new URL("http://localhost:4444/wd/hub");
			
			RemoteWebDriver driver = new RemoteWebDriver(url,new ChromeOptions());
			
			driver.get("https://www.google.com/");
			
			driver.manage().window().maximize();
			
			Thread.sleep(5000);
			
			String current_url = driver.getCurrentUrl();
			
			String current_title = driver.getTitle();
			
			System.out.println("The URL is   : " + current_url);
			
			System.out.println("The Title is : " + current_title);
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}