package com.sales.force;

import org.testng.annotations.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Feature;

public class Account {
	public WebDriver driver;
	public static void wait(int n) {
		try {
			Thread.sleep(n * 1000);
		}catch (Exception e) {
			e.getMessage();
		}
	}
	@Feature("")
	@BeforeTest
	public void driverInitialize() {
		WebDriverManager.chromedriver().arch32().setup();
		ChromeOptions option = new ChromeOptions();
		option.addArguments("--start-maximized");
		option.addArguments("-incognito");
		driver = new ChromeDriver(option);
		driver.get("https://goldcoastitsolutions2.my.salesforce.com/");
		wait(5);
	}
	@Test(priority = 1)
	public void Login() {
		WebElement username = driver.findElement(By.xpath("//input[@name='username']"));
		username.sendKeys("rdinesh808-kzeu@force.com");
		wait(2);
		WebElement password = driver.findElement(By.xpath("//input[@name='pw']"));
		password.sendKeys("Dinesh@123");
		wait(2);
		WebElement login = driver.findElement(By.xpath("//input[@id='Login']"));
		login.click();
		wait(3);
	}
}