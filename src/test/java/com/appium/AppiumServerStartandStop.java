package com.appium;

import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.testng.annotations.*;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class AppiumServerStartandStop {
	AppiumDriverLocalService appiumservice;

	@BeforeSuite
	public void startAppium() {
		AppiumServiceBuilder builder = new  AppiumServiceBuilder();
		builder.withIPAddress("127.0.0.1").build();
		builder.usingPort(4723).build();
		appiumservice = AppiumDriverLocalService.buildService(builder);
		//appiumservice = AppiumDriverLocalService.buildDefaultService();
		appiumservice.start();
		String url = appiumservice.getUrl().toString();
		System.out.println("URL : " + url);
	}
	@AfterSuite
	public void stopAppium() {
		appiumservice.stop();
	}
	@Test
	public void tc_01() {
		System.out.println("Appium");
	}
}