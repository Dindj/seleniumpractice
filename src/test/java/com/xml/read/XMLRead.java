package com.xml.read;

import java.io.*;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLRead {
	public static NodeList xpathexpress(Document doc,String xpathvalue) throws XPathExpressionException {
		XPathFactory xpathfactory = XPathFactory.newInstance();
		XPath xpath = xpathfactory.newXPath();
		XPathExpression expression = xpath.compile(xpathvalue);
		return (NodeList) expression.evaluate(doc, XPathConstants.NODESET);
	}
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		DocumentBuilderFactory buildfactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = buildfactory.newDocumentBuilder();
		Document document = builder.parse(new File("./DataFile/xmldata.xml"));
		
		String rootnode = document.getDocumentElement().getNodeName();
		System.out.println("Root Nose is : " + rootnode);
		NodeList nodes = xpathexpress(document,"//employees/employee/lastName[text()]");
		List<String> list = new ArrayList<>();
		for(int i=0;i<nodes.getLength();i++) {
			Node node = nodes.item(i);
			list.add(node.getTextContent());
		}
		System.out.println("Name : " + list);
		NodeList attname = xpathexpress(document, "//employees/employee/firstName[@name]");
		List<String> attribute = new ArrayList<String>();
		for(int i=0;i<attname.getLength();i++) {
			Node node = attname.item(i).getAttributes().getNamedItem("name");
			String value = node.getTextContent();
			attribute.add(value);
		}
		System.out.println("Attribute Value is : " + attribute);
	}
}