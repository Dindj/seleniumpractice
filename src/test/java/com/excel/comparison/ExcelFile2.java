package com.excel.comparison;

import java.io.*;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelFile2 {
	public void excelfile1() {
		try {
			FileInputStream fis = new FileInputStream(new File("./Excel/ExcelFile2.xlsx"));
			XSSFWorkbook book = new XSSFWorkbook(fis);
			XSSFSheet sheet = book.getSheet("file2");
			int lastrow = sheet.getLastRowNum();
			DataFormatter formatter = new DataFormatter();
			File file = new File("./XMLFile/xmlfile2.xml");
			FileWriter writer = new FileWriter(file);
			writer.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>\n");
			writer.write("<exceldocument>\n");
			writer.write("<sheet>\n");
			writer.write("<text>\n");
			for(int i=1;i<=lastrow;i++) {
				String s1 = "";
				int lastcell = sheet.getRow(i).getLastCellNum();
				for(int j=0;j<=lastcell;j++) {
					XSSFRow row = sheet.getRow(i);
					XSSFCell cell = row.getCell(j, XSSFRow.MissingCellPolicy.CREATE_NULL_AS_BLANK);
					CellAddress address = cell.getAddress();
					if(cell.getCellType()!=CellType.BLANK) {
						s1 = formatter.formatCellValue(cell); 
						writer.write("<textitem celladdress=\""+address+"\">"+s1+"</textitem>\n");
					}
				}
			}
			writer.write("</text>\n");
			writer.write("</sheet>\n");
			writer.write("</exceldocument>\n");
			writer.close();
			book.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		ExcelFile2 excelfile02 = new ExcelFile2();
		excelfile02.excelfile1();
	}
}