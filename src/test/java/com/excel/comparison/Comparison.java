package com.excel.comparison;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.collections4.map.HashedMap;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Comparison {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		ExcelFile1 file1 = new ExcelFile1();
		ExcelFile2 file2 = new ExcelFile2();
		file1.excelfile1();
		file2.excelfile1();
		
		String xmlfilepath1 = "./XMLFile/xmlfile1.xml";
		String xmlfilepath2 = "./XMLFile/xmlfile2.xml";
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document1 = builder.parse(new File(xmlfilepath1));
		XPathFactory xpathfactory = XPathFactory.newInstance();
		XPath xpath = xpathfactory.newXPath();
		XPathExpression expression1 = xpath.compile("//text//textitem[text()]");
		NodeList nodelist1 = (NodeList)expression1.evaluate(document1,XPathConstants.NODESET);
		
		Map<String, String> map = new LinkedHashMap<>();
		for(int i=0;i<nodelist1.getLength();i++) {
			Node node = nodelist1.item(i);
			map.put("value_"+i, node.getTextContent());
		}
		System.out.println(map);
		
		DocumentBuilderFactory factory2 = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder2 = factory2.newDocumentBuilder();
		Document document2 = builder2.parse(new File(xmlfilepath2));
		XPathFactory xpathfactory2 = XPathFactory.newInstance();
		XPath xpath2 = xpathfactory2.newXPath();
		XPathExpression expression2 = xpath2.compile("//text//textitem[text()]");
		NodeList nodelist2 = (NodeList)expression2.evaluate(document2,XPathConstants.NODESET);
		
		Map<String, String> map1 = new LinkedHashMap<>();
		for(int i=0;i<nodelist2.getLength();i++) {
			Node node = nodelist2.item(i);
			map1.put("value_"+i, node.getTextContent());
		}
		System.out.println(map1);
		
		String value1 = null,value2 = null;
		for(Map.Entry<String, String> m : map.entrySet()) {
			boolean flag = false;
			for(Map.Entry<String, String> m1 : map1.entrySet()) {
				 value1 = m.getValue();
				 value2 = m1.getValue();
				if(value1.equals(value2)) {
					flag = true;
					break;
				}
			}
			if(flag==false) {
				System.out.println(value1 + " " + value2);
			}
		}
		
		
	
//		for(int i=0;i<nodelist1.getLength();i++) {
//			boolean flag = false;
//			String s1 = nodelist1.item(i).getTextContent();
//			for(int j=i;j<nodelist2.getLength();j++) {
//				String s2 = nodelist2.item(j).getTextContent();	
//				if(s1.equals(s2)) {
//					flag = true;
//					break;
//				}else {
//					System.out.println(s1 + " " + s2);
//					break;
//				}
//			}
//		}
	}
}
