package com.logical.programs;

import java.util.*;
import com.mifmif.common.regex.*;

public class PrintFormate {
	public void randomwithjar(String s1) {
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<s1.length();i++) {
			char ch1 = s1.charAt(i);
			if(Character.isDigit(ch1)) {
				sb.append("[0-9]");
			}else if (Character.isLetter(ch1)) {
				sb.append("[A-Z]");
			}else {
				sb.append(ch1);
			}
		}
		Generex  gen = new Generex(sb.toString());
		String s2 = gen.random();
		System.out.println("Random Value using Generex jar  : " + s2);
	}
	public void randomwithoutjar(String s1) {
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<s1.length();i++) {
			char ch = s1.charAt(i);
			if(Character.isDigit(ch)) {
				sb.append(new Random().nextInt(9));
			}else if(Character.isLetter(ch)) {
				sb.append(randomchar());
			}
		}
		System.out.println("Random Value using Random Class : " + sb);
	}
	public static char randomchar() {
		String s2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int b = 0;
		for(int i=0;i<1;i++) {
			b = new Random().nextInt(s2.length());
		}
		return s2.charAt(b);
	}
	public static void main(String[] dinesh) {
		Scanner scan = new Scanner(System.in);
		PrintFormate printformate = new PrintFormate();
		System.out.print("Enter the String : ");
		String s1 = scan.next();
		printformate.randomwithjar(s1);
		printformate.randomwithoutjar(s1);
		scan.close();
	}
}
