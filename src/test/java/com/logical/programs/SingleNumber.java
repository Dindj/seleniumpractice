package com.logical.programs;

public class SingleNumber {
	public String reverseWords(String s) {
		String s1[] = s.split(" ");
		String s3 = "";
		for(int i=s1.length-1;i>=0;i--) {
			if(!s1[i].isEmpty()) {
				String s2 = s1[i].trim();
				s3 += s2 + " ";
			}
		}
		return s3.trim();
	}
	public static void main(String[] args) {
		SingleNumber number = new SingleNumber();
		System.out.println(number.reverseWords("Alice does not even like bob"));
	}
}