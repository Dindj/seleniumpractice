package com.logical.programs;

public class Pattern {
	public void stringattern(String str) {
		char ch[] = str.toCharArray();
		for(int i=0;i<ch.length;i++) {
			for(int j=0;j<=i;j++) {
				System.out.print(ch[j]);
			}
			System.out.println();
		}
	}
	public void stringPattern(String str) {
		char[] ch = str.toCharArray();
		for(int i=0;i<ch.length;i++) {
			for(int j=0;j<(ch.length-i);j++) {
				System.out.print(" ");
			}
			for(int k=0;k<=i;k++) {
				System.out.print(ch[k]);
			}
			System.out.println();
		}
	}
	public void stringPatterns(String str) {
		char[] ch = str.toCharArray();
		for(int i=0;i<ch.length;i++) {
			for(int j=0;j<(ch.length-i);j++) {
				System.out.print(" ");
			}
			for(int k=0;k<=i;k++) {
				System.out.print(ch[k]);
			}
			for(int l=i-1;l>=0;l--) {
				System.out.print(ch[l]);
			}
			System.out.println();
		}
	}
	public static void main(String[] args) {
		Pattern pattern = new Pattern();
		pattern.stringPatterns("Welcome");
	}
}