package com.logical.programs;

import java.util.Arrays;

public class PlusOne {
	public int[] plusOne(int[] digits) {
		int n1[] = new int[digits.length];
		for(int i=0;i<digits.length;i++) {
			if(i==(digits.length-1)) {
				n1[i] = digits[i]+1;
				String s1 = String.valueOf(n1[i]);
				char[] ch = s1.toCharArray();
				if(s1.length()!=1) {
					n1 = new int[s1.length()];
					for(int j=0;j<ch.length;j++) {
						n1[i] = ch[i];
					}
				}
			}else {
				n1[i] = digits[i];
			}
		}
		return n1;
	}
	public static void main(String[] args) {
		PlusOne plus_one = new PlusOne();
		int n[] = {9};
		System.out.println(Arrays.toString(plus_one.plusOne(n)));
	}
}