package com.logical.programs;

import java.util.Arrays;

public class SortColors {
	public void sortColors(int[] nums) {
		Arrays.sort(nums);
		System.out.println(Arrays.toString(nums));
	}
	public static void main(String[] args) {
		SortColors sortcolor = new SortColors();
		int[] n = {2,0,2,1,1,0};
		sortcolor.sortColors(n);
	}
}