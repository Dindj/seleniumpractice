package com.logical.programs;

import java.util.*;

public class RemoveDuplicatesfromSortedArray {
	public int removeDuplicates(int[] nums) {
		Set<Integer> set = new LinkedHashSet<>();
		for(int i=0;i<nums.length;i++) {
			set.add(nums[i]);
		}
		int n = set.size();
		return n;
	}
	public static void main(String[] args) {
		RemoveDuplicatesfromSortedArray removeduplicates = new RemoveDuplicatesfromSortedArray();
		int n[] = {0,0,1,1,1,2,2,3,3,4};
		System.out.println(removeduplicates.removeDuplicates(n));
	}
}