package com.logical.programs;

public class CountPrimeNumbers {
	public int countPrimes(int n) {
		int count = 0;
		for(int i=2;i<=n;i++) {
			if(i%2==1) {
				count++;
			}
		}
		return count;
	}
	public static void main(String[] args) {
		CountPrimeNumbers primenumbers = new CountPrimeNumbers();
		System.out.println(primenumbers.countPrimes(1));
	}
}