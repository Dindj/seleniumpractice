package com.logical.programs;

public class ReverseInteger {
	public int reverse(int x) {
		String s1 = String.valueOf(x);
		char ch[] = s1.toCharArray();
		String s2 = "";
		if(!Character.isDigit(ch[0])) {
			s2 += ch[0];
		}
		for(int i=(ch.length-1);i>=0;i--) {
			if(Character.isDigit(ch[i])) {
				s2 += ch[i];
			}
		}
		return Integer.parseInt(s2);
	}
	public static void main(String[] args) {
		ReverseInteger reverse = new ReverseInteger();
		System.out.println(reverse.reverse(120));
	}
}