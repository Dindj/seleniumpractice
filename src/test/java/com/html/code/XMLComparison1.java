package com.html.code;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLComparison1 {
	public String File1 = "./XMLFile/xmlfile1.xml";
	List<String> list1 = new ArrayList<String>();
	public List<String> xmlfile1() {
		try {
			DocumentBuilderFactory buildfactory = DocumentBuilderFactory.newDefaultInstance();
			DocumentBuilder builder = buildfactory.newDocumentBuilder();
			Document document = builder.parse(new File(File1));

			XPathFactory xpathfactory = XPathFactory.newDefaultInstance();
			XPath xpath = xpathfactory.newXPath();
			XPathExpression expression = xpath.compile("//text/textitem[text()]");
			NodeList nodelist = (NodeList)expression.evaluate(document,XPathConstants.NODESET);
			for(int i=0;i<nodelist.getLength();i++) {
				Node node = nodelist.item(i);
				String textvalue = node.getTextContent();
				list1.add(textvalue.trim());
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return list1;
	}
}