package com.html.code;

import java.io.*;
import java.util.*;
import org.xml.sax.SAXException;
import javax.xml.transform.TransformerConfigurationException;

public class HTMLWithJava {
	public static void main(String[] args) throws IOException, TransformerConfigurationException, SAXException {

		List<String> file1 = new XMLComparison1().xmlfile1();
		List<String> file2 = new XMLComparison2().xmlfile2();

		BufferedWriter bw = new BufferedWriter(new FileWriter("./HTML_FILE/One.html"));
		bw.write("<html>\n");
		bw.write("<head>\n");
		bw.write("<title>New Page</title>\n");
		bw.write("<style>\ntable, th, td {\r\n"
				+ "  border: 1px solid black;\r\n"
				+ "  border-collapse: collapse;\r\n"
				+ "}\n"
				+ ".tab,tab1s{\n"
				+ "top:30px;\n"
				+ "}\n"
				+ "</style>\n");
		bw.write("</head>\n");
		bw.write("<body>\n");
		bw.write("<div align=\"left\">");
		bw.write("<table>\n");
		for(int i=0;i<file1.size();i++) {
			for(int j=i;j<file2.size();) {
				if(!file1.get(i).equals(file2.get(j))) {
					bw.write("<tr>\n");
					bw.write("<td>"+file1.get(i)+"</td>\n");
					bw.write("</tr>\n");	
				}	
				break;
			}
		}
		bw.write("\n");
		bw.write("</table>\n");
		bw.write("</div>");
		bw.write("<div align=\"right\">");
		bw.write("<table>\n");
		for(int i=0;i<file2.size();i++) {
			for(int j=i;j<file1.size();) {
				if(!file2.get(i).equals(file1.get(j))) {
					bw.write("<tr>\n");
					bw.write("<td>"+file2.get(i)+"</td>\n");
					bw.write("</tr>\n");	
				}	
				break;
			}
		}
		bw.write("\n");
		bw.write("</table>\n");
		bw.write("</div>");
		bw.write("</head>\n");
		bw.write("</html>\n");
		bw.close();
		System.out.println("Success...");
	} 
}