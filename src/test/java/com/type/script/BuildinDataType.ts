//Number
var a:number=10;
console.log(a);
var b:number=0x37CF;
console.log(b);
var c:number=0o377;
console.log(c);
var d:number=12.432
console.log(d);
var e:number=0b111001;
console.log(e);

//String
var username:string="John";
var userdept:string="IT";
console.log(username);
console.log(userdept);
var concat:string=username+" Works in " + userdept;
console.log(concat);

//Boolean
var bool:boolean=true;
console.log(bool);

//void type
function hello():void {
    console.log("This is void data type");
    // void not return value
}
hello();

//Null
var num1:number=null;
num1 = 1000;
console.log(num1);

//Undefined
var num2:number=undefined; // premitive type
num2 = 2000;
console.log(num2);

//any type
var val:any="Type Script";
val = 100;
console.log(val);