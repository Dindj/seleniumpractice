var Greeter = /** @class */ (function () {
    function Greeter() {
    }
    Greeter.prototype.great = function () {
        console.log("Welcome to Type Script");
    };
    return Greeter;
}());
var obj = new Greeter();
obj.great();
