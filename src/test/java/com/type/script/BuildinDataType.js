//Number
var a = 10;
console.log(a);
var b = 0x37CF;
console.log(b);
var c = 255;
console.log(c);
var d = 12.432;
console.log(d);
var e = 57;
console.log(e);
//String
var username = "John";
var userdept = "IT";
console.log(username);
console.log(userdept);
var concat = username + " Works in " + userdept;
console.log(concat);
//Boolean
var bool = true;
console.log(bool);
//void type
function hello() {
    console.log("This is void data type");
    // void not return value
}
hello();
//Null
var num1 = null;
num1 = 1000;
console.log(num1);
//Undefined
var num2 = undefined; // premitive type
num2 = 2000;
console.log(num2);
//any type
var val = "Type Script";
val = 100;
console.log(val);
