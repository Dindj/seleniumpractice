class Greeter {
    great()
    : void
    {
        console.log("Welcome to Type Script");
    }
}

var obj  = new Greeter();
obj.great();