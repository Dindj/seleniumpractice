function one() {
    var flag = true;
    if (flag == true) {
        var x = 100;
        var y = 200;
        console.log(x);
        console.log(y);
    }
    console.log(x);
    //   console.log(y); //error - only use with in the block
}
one();
var a = 10;
console.log(a);
var a = 20;
console.log(a);
a = 30;
console.log(a);
var b = 40;
console.log(b);
//let b = 50;
//console.log(b); // shown error already defind
var c = 100;
console.log(c);
//c = 200;
//console.log(c); // shown error, because c value fixed.s
// var - keyword use only within the function(update or redefind)
// let - leyword use only within the block
//const - must declare a const variable with an initial value
// let ane const - cannot update and redefind
